<?php

use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * initSession
 * Used in /account/login and /account/create
 *
 * @param  Illuminate\Http\Request  $request
 * @param  Object  $user
 * @return void
 */
function initSession(&$request,$user)
{
    $sid = md5(time());
    if (!$request->cookie('sid'))
    {
        setcookie('sid', $sid, time()+60*60*24*30);
    }
    else
    {
        $sid = $request->cookie('sid');
    }
    $session = DB::table('sessions')->where('sid',$sid)->first();
    if (is_null($session))
    {
        DB::table('sessions')->insert([ 'uid' => $user->id, 'sid' => $sid ]);
    }
}

$app->group(['prefix' => 'link'], function () use ($app) {

    /**
     * /link POST To create a new link
     * @return Boolean|Object   false if fail the link object if fine
     */
    $app->post('/', ['middleware' => 'auth', function (Request $request) use ($app) {
        $user = $request->user();
        $this->validate($request, [ 'link' => 'required|min:4' ]);
        $link = $request->input('link');
        $end = DB::table('links')->insert([
            'owner' => $user->id,
            'link' => $link
        ]);
        if ( $end === true )
        {
            $link = DB::table('links')
                ->where('link', $link)
                ->where('owner', $user->id)
                ->join('users', 'owner', '=', 'users.id')
                ->first();
            return response()->json( $link );
        }
        return response()->json( $end );
    }]);

    /**
     * /link/{id} POST for voting
     * @return Integer  0 if fail 1 if fine
     */
    $app->post('/{id}/{method}', ['middleware' => 'auth', function (Request $request, $id, $method) use ($app) {

        $user = $request->user();

        // Query database to find possible user vote
        $voted = DB::table('votes')
            ->where('user', $user->id)
            ->where('link', $id)
            ->where('method', $method)
            ->first();

        $res = 0; // this is the result of the operation

        // if voted is empty continue
        if (empty($voted))
        {

            // Increment vote count
            if($method==='plus'){
                $res = DB::table('links')
                    ->where('id', $id)
                    ->increment('votes', 1);
            }

            if($method==='minus'){
                $res = DB::table('links')
                    ->where('id', $id)
                    ->decrement('votes', 1);
            }

            // Create vote in votes table
            DB::table('votes')->insert([
                'user' => $user->id,
                'link' => $id,
                'method' => $method
            ]);

        }
        // Return vote with the result
        return response()->json( $res );
    }]);

    /**
     * /link/{id} DELETE for link
     * @return Boolean  false if fail true if fine
     */
    $app->delete('/{id}', ['middleware' => 'auth', function (Request $request, $id) use ($app) {
        $user = $request->user();
        // Find and delete
        $end = DB::table('links')
            ->where('owner', '=', $user->id)
            ->where('id', '=', $id)
            ->delete();
        if ( is_numeric($end) && $end > 0 )
        {
            return response()->json( true );
        }
        return response()->json( false );
    }]);

});

$app->group(['prefix' => 'account'], function () use ($app) {

    $app->get('{lang}', function (Request $request, $lang) use ($app) {
        $user = $request->user();
        $out = [ 'lang' => $lang, 'user' => $user ];
        $response = new Response( view( 'profile', $out ) );
        return $response;
    });

    $app->get('logout/{lang}', function (Request $request, $lang) use ($app)
    {
        $user = $request->user();
        if(!empty($user)){
            $sid = $request->user()->sid;
            DB::table('sessions')->where('sid', $sid)->delete();
        }
        return redirect('/');
    });

    $app->post('/', function (Request $request) use ($app) {
        $this->validate($request, [
            'name'      => 'min:4',
            'password'  => 'min:4',
            'email'     => 'email'
        ]);
        $user       = $request->user();
        $name       = $request->input('name');
        $email      = $request->input('email');
        $password   = $request->input('password');
        $out        = [];
        $update     = 0;
        if (strlen($password) > 0)
        {
            $hasher     = app()->make('hash');
            $password   = $hasher->make($request->input('password'));
            $out['password'] = $password;
        }
        if (strlen($name) > 0 && $user->name !== $name)
        {
            $out['name'] = $name;
        }
        if (strlen($email) > 0 && $user->email !== $email)
        {
            $out['email'] = $email;
        }
        if (sizeof($out)>0)
        {
            $update = DB::table('users')
                ->where('id', $user->id)
                ->update( $out );
        }
        $out['update'] = $update;
        if (strlen($password) > 0 && $update > 0)
        {
            $out['password'] = true;
        }
        $out['lang'] = $lang;
        return response()->json( $out );
    });

    $app->post('forgot', function (Request $request) use ($app) {
        return response()->json( false );
    });

    $app->post('login', function (Request $request) use ($app)
    {

        $this->validate($request, [
            'password'  => 'required|min:4',
            'email'     => 'required|email'
        ]);

        // Start a Hash instance.
        $hasher     = app()->make('hash');

        $email      = $request->input('email');

        // This is one of the missing parts.
        $password   = $hasher->make($request->input('password'));

        // Find user by email.
        // In Node under most of patterns you get both email and password during this step
        $user       = DB::table('users')->where('email',$email)->first();

        // Check using hasher
        $check      = $hasher->check($request->input('password'),$user->password);

        // If everything is fine, call initSession and send the proper information
        if ($check === TRUE)
        {
            initSession($request, $user);
            return response()->json( [ true, $user->name ] );
        }
        else
        {
            return response()->json( [ false ] );
        }
    });

    $app->post('create', function (Request $request) use ($app)
    {

        $this->validate($request, [
            'password' => 'required|min:4',
            'email' => 'required|email|unique:users',
            'name' => 'required|unique:users'
        ]);

        $name = $request->input('name');
        $email = $request->input('email');

        // Hash password
        $password = app()->make('hash')->make($request->input('password'));

        // Insert user into DB
        // This step is basically the same pattern in PHP and Node
        $insert = DB::table('users')->insert([
            'password' => $password,
            'email' => $email,
            'name' => $name,
            'created_at' => 'NOW()',
            'updated_at' => 'NOW()'
        ]);

        // If insert is true, find the new user and call initSession
        // and finish the operation
        if($insert === TRUE)
        {
            $user = DB::table('users')->where('email', $email)->first();
            initSession($request, $user);
            return response()->json( [ $insert, $name ] );
        }

        // If not continue the operation and return fail
        return response()->json( [ false ] );

    });

});

$app->get('/{lang}', function (Request $request, $lang) use ($app) {

    $session = $request->user();

    if (!empty($session))
    {
        $out['user'] = $session;
    }

    $out['links'] = DB::table('links')
        ->join('users', 'owner', '=', 'users.id')
        ->select('links.*', 'links.id as link_id', 'users.id', 'users.name')
        ->orderBy('votes', 'desc')
        ->paginate(10);
        // ->get();

    $out['lang'] = $lang;

    $response = new Response( view( 'index', $out ) );

    return $response;

});

$app->get('/', function (Request $request) use ($app) {
    $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
    if(!$request->cookie('sid')){ setcookie('sid', md5(time()), time()+60*60*24*30); }
    switch ($lang){
        case "en":
            return redirect('/en');
            break;
        case "es":
            return redirect('/es');
            break;
        default:
            return redirect('/en');
            break;
    }
});

/*

// This is not the proper way to implement multi-language in Lumen
// Wasted some time in this...

$app->get('/en', function (Request $request) use ($app) {
    $response = new Response( view( 'index' );
    return $response;
});

$app->get('/es', function () use ($app) {
    return view('index');
});

*/



