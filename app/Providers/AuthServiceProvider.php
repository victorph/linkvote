<?php

namespace App\Providers;

use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

	/**
	 * Boot the authentication services for the application.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->app['auth']->viaRequest('api', function ($request)
		{
			$sid = $request->cookie('sid');
			if(!empty($sid))
			{
				$u = $this->app['db']
					->table('sessions')
					->where('sid',$sid)
					->join('users','uid','=','users.id')
					->select('users.id', 'users.name', 'users.email', 'sessions.sid as sid')
					->first();
				return $u;
			}
		});
	}
}
