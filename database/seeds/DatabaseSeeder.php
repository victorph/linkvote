<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$hasher = app()->make('hash');
		DB::table('users')->insert( array(
			'name' => 'Root',
			'email' => 'root@root.com',
			'password' => $hasher->make('12345')
			)
		);
	}
}
