
# Link Vote

#### Intro ####

Hello and thank you for your time, Now I will try to explain, And narrate my experience
doing this project

I have to say that this is the most PHP I have done in the past 10 years of my life, So
I spent around 12 hours doing this, Learning Laravel Lumen, And remembering PHP, It was fun

Most the time was spent learning my way around the framework... And sometimes failing to find it

After a while of using it more things became clear, And I started to try to do things the way
Laravel Lumen would normally do, And I tried the best I could to use all parts of the framework
but a few thing eluded me from the documentation, stack overflow questions, or my own intuition

##### List things I could not figure out partially or entirely #####

* How to do proper authentication (I believe I invented my own approach...)
* How to use named routes and point to controllers (Entirely missed out how to use this...)
* How to properly use the models (Same as above)
* How to use cookies (Partially... sad... ugh)

I really did not explore much the use of Eloquent

#### Environment ####

The only extra or special environmental value is APP_BRAND, which holds the name

#### TODO ###

* Confirmations
* Forgotten password

#### Security issues ####

* I'm not generating a secret token during the login, I'm only using the a cookie named sid for all operations, So the session is very easy to steal

* Some further configuration to Laravel Lumen might be needed, But a lot of security problems are solver with the proper implementation of Docker

#### Assets ####

I used jQuery because It's better known among the web developers, And also because I'm very used to it

Also used compiled assets from other project:

	public/css/app.min.css
	public/js/app.lib.js

Containing many CSS, and JavaScript libraries, That's why the js file is 809 KB, And the CSS file 205 KB

The JavaScript code for the front-end:

	public/js/app.js

It contains detailed information in how everything operates

#### Conclusion ####

I really liked this new experience of working with the Laravel Lumen minimal Framework,
I find it very complete, clear and it covers so many issues of web development, I can
totally understand why is the Framework of choice at CodeUP

I will continue studying it, And will add it to my tool bet, AS I believe there is a few tools
for each job at had

I'm looking at this as a prototype, Not a finished product, I'm not sure... :)

#### List of related topics ####

* [Docker] - More about Docker

[Docker]: https://semaphoreci.com/community/tutorials/dockerizing-a-php-application
