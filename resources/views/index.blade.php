<html>

	<head>
		<title><?php echo($_ENV['APP_BRAND']); ?></title>
		<link href="/css/app.min.css" rel="stylesheet" type="text/css"/>
	</head>

	<body>

		@include('navbar')

		@include('modals')

		<div class="container">

			<div class="row" data-translate="true">
				<div class="col-xs-4">
					<h3>
						<span data-i18n="welcome_pt1"></span> <span>{{ $_ENV['APP_BRAND'] }}</span>
					</h3>
					<span data-i18n="welcome_pt2"> </span><br>
				</div>
				<div class="col-xs-8">
					<img src="http://placekitten.com/g/470/200" class="img-responsive">
				</div>
			</div>
			<br>

			<div class="row" data-translate="true">
				<br>
				<div class="container">
					<button id="actionAddLink" type="button" class="hidden col-xs-12 btn btn-success" data-i18n="add_link"></button>
				</div>
				<br><br><br>
			</div>

			<div class="row" data-translate="true" style="text-align:center">
				<div class="col-xs-2 header-actions hidden"><b data-i18n="actions"></b></div>
				<div class="col-xs-1"><b data-i18n="votes"></b></div>
				<div class="col-xs-3"><b data-i18n="poster"></b></div>
				<div class="col-xs-6"><b data-i18n="link"></b></div>
			</div>

			<div class="container" id="linkList">

				<?php
					if(!empty($links)){
						$cname="hidden";
						$_uid=0;
						if(!empty($user)){
							$cname="";
							$_uid=$user->id;
						}
					}
				?>

				@foreach ($links as $link)
				<div class="row">
					<div class="col-xs-2 link-ctrls hidden">
						<a href="#" class="link-vote {{ $cname }}" data-method="plus" data-link="{{ $link->link_id }}" data-owner="{{ $_uid }}">
							<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
						</a>
						<a href="#" class="link-vote {{ $cname }}" data-method="minus" data-link="{{ $link->link_id }}" data-owner="{{ $_uid }}">
							<span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
						</a>
						<a href="#" class="link-delete {{ $cname }}" data-link="{{ $link->link_id }}" data-owner="{{ $_uid }}">
							<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
						</a>
					</div>
					<div class="col-xs-1" id="link_votes"> {{ $link->votes }} </div>
					<div class="col-xs-3"> {{ $link->name }} </div>
					<div class="col-xs-6" id="link_link">
						<a href="//{{ $link->link }}" target="_blank">{{ $link->link }}</a>
					</div>
				</div>
				@endforeach

			</div>

			<br>

			<div class="row" data-translate="true" style="text-align:left">
				{!! $links->links() !!}
			</div>

		</div>

		<script type="text/javascript" src="/js/app.libs.js"></script>

		<script type="text/javascript" src="/js/app.js"></script>

		<script type="text/javascript">
			'use strict';
			var i18n_conf = { lng: '<?php echo $lang ?>' }
			new InitI18next(i18n_conf);
		</script>

		@unless (empty($user))
			<script type="text/javascript">
				'use strict';
				$(document).ready(function(){
					var sto = setTimeout(function(){
						_setNickname("<?php echo $user->name ?>");
						_toggleUserMenuActions();
						$('#actionAddLink').removeClass('hidden');
						__refreshUIAfterUser();
						return clearTimeout(sto);
					},600);
					return;
				});
			</script>
		@endunless

		<template id="link_temp" class="hidden">
			<div class="row">
				<div class="col-xs-2 link-ctrls">
					<a href="#" class="link-vote" data-method="plus">
						<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
					</a>
					<a href="#" class="link-vote" data-method="minus">
						<span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
					</a>
					<a href="#" class="link-delete">
						<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
					</a>
				</div>
				<div class="col-xs-1" id="link_votes">&nbsp;</div>
				<div class="col-xs-3" id="link_name">&nbsp;</div>
				<div class="col-xs-6"> <a href="#" target="_blank" id="link_link">&nbsp;</a> </div>
			</div>
		</template>

	</body>

</html>