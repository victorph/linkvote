<html>

	<head>
		<title>{{ $_ENV['APP_BRAND'] }} - Profile</title>
		<link href="/css/app.min.css" rel="stylesheet" type="text/css"/>
	</head>

	<body>

		@include('navbar')

		@include('modals')

		<div class="container" data-translate="true">
			<form id="FormProfileUpdate" action="/account" method="post">
				<div class="form-group">
					<label for="inputProfileName" data-i18n="name"></label>
					<input name="name" type="text" class="form-control" id="inputProfileName" value="{{ $user->name }}">
				</div>
				<div class="form-group hidden">
					<label for="inputProfileEmail" data-i18n="email"></label>
					<input name="email" type="email" class="form-control" id="inputProfileEmail" value="{{ $user->email }}">
				</div>
				<div class="form-group">
					<label for="inputProfileEmail" data-i18n="password"></label>
					<input name="password" type="password" class="form-control" id="inputProfilePassword" value="">
				</div>
				<button type="reset" class="btn btn-default" data-i18n="reset"></button>
				<button type="submit" class="btn btn-primary" data-i18n="submit"></button>
			</form>
		</div>

		<script type="text/javascript" src="/js/app.libs.js"></script>

		<script type="text/javascript" src="/js/app.js"></script>

		<script type="text/javascript">
			'use strict';
			var i18n_conf = { lng: '{{ $lang }}' }
			new InitI18next(i18n_conf);
			$(document).ready(function(){
				var sto = setTimeout(function(){
					_setNickname("{{ $user->name }}");
					_toggleUserMenuActions();
					return clearTimeout(sto);
				},600);
				return;
			});
		</script>

	</body>

</html>