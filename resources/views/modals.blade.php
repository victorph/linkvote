
<form id="FormLogin" action="/account/login" method="post">
	<div id="UIModalLogin" data-translate="true" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" data-i18n="login"></h4>
				</div>

				<div class="modal-body">

					<div class="form-group">
						<label for="loginInputEmail" data-i18n="email_lg"></label>
						<input name="email" type="email" class="form-control" id="loginInputEmail" data-i18n="[placeholder]email">
					</div>

					<div class="form-group">
						<label for="loginInputPassword" data-i18n="password"></label>
						<input name="password" type="password" class="form-control" id="loginInputPassword" data-i18n="[placeholder]password">
					</div>

				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" data-i18n="submit"></button>
					<button type="reset" class="btn btn-default" data-dismiss="modal" data-i18n="close"></button>
				</div>

			</div>
		</div>
	</div>
</form>

<form id="FormCreate" action="/account/create" method="post">
	<div id="UIModalCreate" data-translate="true" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" data-i18n="create"></h4>
				</div>

				<div class="modal-body">

					<div class="form-group">
						<label for="createInputName" data-i18n="name"></label>
						<input name="name" type="name" class="form-control" id="createInputName" data-i18n="[placeholder]name">
					</div>

					<div class="form-group">
						<label for="createInputEmail" data-i18n="email_lg"></label>
						<input name="email" type="email" class="form-control" id="createInputEmail" data-i18n="[placeholder]email">
					</div>

					<div class="form-group">
						<label for="createInputPassword" data-i18n="password"></label>
						<input name="password" type="password" class="form-control" id="createInputPassword" data-i18n="[placeholder]password">
					</div>

				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" data-i18n="submit"></button>
					<button type="reset" class="btn btn-default" data-dismiss="modal" data-i18n="close"></button>
				</div>

			</div>
		</div>
	</div>
</form>

<form id="FormForgot" action="/account/forgot" method="post">
	<div id="UIModalForgot" data-translate="true" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" data-i18n="forgot"></h4>
				</div>

				<div class="modal-body">

					<div class="form-group">
						<label for="forgotInputEmail" data-i18n="email_lg"></label>
						<input name="email" type="email" class="form-control" id="forgotInputEmail" data-i18n="[placeholder]email">
					</div>

				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" data-i18n="submit"></button>
					<button type="reset" class="btn btn-default" data-dismiss="modal" data-i18n="close"></button>
				</div>

			</div>
		</div>
	</div>
</form>

<form id="FormLinkCreate" action="/link" method="post">
	<div id="UIModalLinkCreate" data-translate="true" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" data-i18n="add_link"></h4>
				</div>

				<div class="modal-body">

					<div class="form-group">
						<label for="linkInputLink" data-i18n="link"></label>
						<input name="link" type="text" class="form-control" id="linkInputLink" data-i18n="[placeholder]link_example">
					</div>

				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" data-i18n="submit"></button>
					<button type="reset" class="btn btn-default" data-dismiss="modal" data-i18n="close"></button>
				</div>

			</div>
		</div>
	</div>
</form>