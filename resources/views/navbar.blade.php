<nav class="navbar navbar-inverse">

	<div class="container-fluid">

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/<?php echo($lang); ?>" id="UIBrand">{{ $_ENV['APP_BRAND'] }}</a>
		</div>

		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

			<ul class="nav navbar-nav navbar-right" id="UILoginCtrls" data-translate="true">
				<li class="dropdown" id="userMenu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span data-i18n="account" id="userMenuName"></span> <span class="caret"></span></a>
					<ul class="dropdown-menu" id="userMenuActions">
						<li> <a href="#" onclick="$('#UIModalCreate').modal('show'); return false;" data-i18n="create"></a> </li>
						<li> <a href="#" onclick="$('#UIModalLogin').modal('show'); return false;" data-i18n="login"></a> </li>
						<li role="separator" class="divider"></li>
						<li> <a href="#" onclick="$('#UIModalForgot').modal('show'); return false;" data-i18n="forgot"></a> </li>
						<li class="hidden"> <a href="/account/{{ $lang }}" data-i18n="profile"></a> </li>
						<li class="hidden"> <a href="/account/logout/{{ $lang }}" data-i18n="logout"></a> </li>
					</ul>
				</li>
			</ul>

			<ul class="nav navbar-nav navbar-right" role="search" data-translate="true">
				@if ($lang !== 'en')
				<li> <a href="/en"><img src="/img/us.png"></a> </li>
				@endif
				@if ($lang !== 'es')
				<li> <a href="/es"><img src="/img/es.png"></a> </li>
				@endif
			</ul>

			<form class="navbar-form navbar-right hidden" role="search" data-translate="true">
				<div class="form-group">
					<input type="text" class="form-control" data-i18n="[placeholder]search">
				</div>
			</form>

		</div>

	</div>

</nav>