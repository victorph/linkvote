'use strict';

/* global _:true */
/* global i18next:true */
/* global $:true */
/* global jqueryI18next:true */

var i18n_conf={ lng: 'en' };

// This is used for the UI, But it's yet to be connected to Laravel Lumen
function InitI18next(opts, cb) {
	var conf = { "debug": false, "lng": 'en', loadPath: '/locales/{{lng}}.json' };
	conf = _.merge(conf, opts);
	return i18next.use(window.i18nextXHRBackend).init(conf, function(err){
		if (err) {
			if (_.isFunction()){ cb(err); }
			throw new Error(err);
		}
		jqueryI18next.init(i18next, $, {
			handleName: 'localize',
			selectorAttr: 'data-i18n',
			targetAttr: 'i18n-target',
			optionsAttr: 'i18n-options',
			useOptionsAttr: false,
			parseDefaultValueFromContent: true
		});
		_.each($('*[data-translate="true"]'),function(el){ $(el).localize(); });
		return;
	});
}

// Handles link vote
// res should be integer above 0 if everything is fine
function handleLinkVote (res, method, el) {
	if ( res > 0 )
	{
		el = el.parent().parent().find('#link_votes');
		var value = parseInt(el.text());
		if (method==='plus'){ value = value + 1; }
		if (method==='minus'){ value = value - 1; }
		el.text(value);
	}
}

// Handles link delete
// res should be true or false
function handleLinkDelete (res, el) {
	if(res === true)
	{
		el = el.parent().parent().remove();
	}
}

// Hooks for the actions in the links
// I could had joined this two in a single function
// But I left it like this for
function refreshLinkActions () {

	// Uses a data attribute called link to get the id and other named method
	// method can be plus or minus
	$('.link-vote').off('click').on('click',function(ev){
		ev.preventDefault();
		var $el = $(this);
		$.ajax( {
			url: '/link/' + $el.data('link') + '/' + $el.data('method'),
			method: 'POST',
			complete: function(res) { return handleLinkVote(res.responseJSON, $el.data('method'), $el); }
		});
		return;
	});

	// Uses a data attribute called link to get the id
	$('.link-delete').off('click').on('click',function(ev){
		ev.preventDefault();
		var $el = $(this);
		$.ajax( {
			url: '/link/' + $el.data('link'),
			method: 'DELETE',
			complete: function(res) { return handleLinkDelete(res.responseJSON, $el); }
		});
		return;
	});

	return;

}

function _inputsColorReset(target){
	$('#'+target).find('input').css('border-color','rgb(204,204,204)');
}

function _inputsColorRed(target, list){
	_inputsColorReset(target);
	return _.each(list,function(msg,it){
		var $el=$('#'+target).find('input[name="'+it+'"]');
		$el.css('border-color','red');
	});
}

// Used to change the text in the drop down of the navigation bar
function _setNickname(str){
	$('#userMenuName').text(str);
}

// Used to change the list of content of userMenuActions
function _toggleUserMenuActions(){
	$('#userMenuActions').children().toggleClass('hidden');
}

function __refreshUIAfterUser () {
	$('.link-ctrls').children().removeClass('hidden');
	$('.link-ctrls').removeClass('hidden');
	$('.header-actions').removeClass('hidden');
	$('#actionAddLink').removeClass('hidden');
	return;
}

// Handles the response of the login
// expects an array if fine and array should
// be [ bool, string ],
// first param is the result of the login
// the second param being the nickname
function handleFormLoginResponse (res) {
	if( _.isArray(res) && res[ 0 ] === true ) {
		_setNickname(res[ 1 ]);
		$('#UIModalLogin').modal('hide');
		_inputsColorReset('FormLogin');
		_toggleUserMenuActions();
		__refreshUIAfterUser();
		return;
	}
	else if ( _.isArray(res) && res[ 0 ] === false ) {
		_inputsColorRed('FormLogin', { 'email': true, 'password': true } );
	}
	else
	{
		_inputsColorRed('FormLogin', res );
	}
	return;
}

// Handle the user creation
// expects an array if fine and array should
// be [ bool, string ],
// first param is the result of the login
// the second param being the nickname
function handleFormCreateResponse (res){
	if(res[ 0 ]===true){
		_setNickname(res[ 1 ]);
		$('#UIModalCreate').modal('hide');
		_toggleUserMenuActions();
		__refreshUIAfterUser();
		return;
	}
	return _inputsColorRed('FormCreate', res );
}

// TODO
function handleFormForgotResponse (res) {
	console.log(res);
}

// Handles link addition
function handleFormLinkCreateResponse (res) {
	console.log(res);
	if (_.isBoolean(res) === true && res === fale) {
		alert('Error while creating link!');
	}
	else {
		// Grab the template in index.php
		// Inserts all content
		// Adds the result to the link list
		var temp = $('#link_temp')[0].innerHTML;
		var $nl = $(temp);
		$nl.find('#link_votes').text(0);
		$nl.find('#link_link').attr('href','//'+res.link);
		$nl.find('#link_link').text(res.link);
		$nl.find('#link_name').text(res.name);
		$nl.find('.link-ctrls').find('a').data('link', res.id);
		$nl.find('.link-ctrls').find('a').data('owner', res.owner);
		$('#UIModalLinkCreate').modal('hide');
		$('#linkList').prepend($nl);
		refreshLinkActions();
		return;
	}
	return _inputsColorRed('FormLinkCreate', res );
}

$(document).ready(function(){

	// This action calls to resets the borders of the
	// any inputs that were marked red
	$('button[type="reset"]').on('click',function(ev){
		return _inputsColorReset(ev.target.form.id);
	});

	// This handles all the forms, And redirects the results
	// Whatever are them to the respective function.
	$('form').submit(function(ev){
		ev.preventDefault();
		var $form = $(ev.target);
		var $method = 'POST';
		if ( ! _.isEmpty( $form.data('method') ) )
		{
			$method = $form.data('method');
		}
		$.ajax( {
			url: $form.attr('action'),
			type: $method,
			data: $form.serialize(),
			complete: function(res) {
				if(ev.target.id==='FormLinkCreate')
				{
					return handleFormLinkCreateResponse(res.responseJSON);
				}
				if(ev.target.id==='FormLogin')
				{
					return handleFormLoginResponse(res.responseJSON);
				}
				if(ev.target.id==='FormCreate')
				{
					return handleFormCreateResponse(res.responseJSON);
				}
				if(ev.target.id==='FormForgot')
				{
					return handleFormForgotResponse(res.responseJSON);
				}
			}
		} );
	});

	// Shows the add link modal.
	$('#actionAddLink').click(function(ev){
		ev.preventDefault();
		$('#UIModalLinkCreate').modal('show');
		return;
	});

	// Call the hooks for the actions
	refreshLinkActions();

	return;

});